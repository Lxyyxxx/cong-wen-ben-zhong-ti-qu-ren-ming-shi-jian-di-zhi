# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/2/13 22:24.
#

import jieba.posseg
import cpca
import re


class Info:
    def __init__(self):
        # 提取日期
        self.dateMark = ['eng', 'date']
        self.concatDate: bool = False
        self.hasDate: bool = False
        self.date = str()
        self.dateTemp = str()
        # 提取时间
        self.timeMark = ['eng', 'time', 'm']
        self.concatTime: bool = False
        self.hasTime: bool = False
        self.time = str()
        self.timeTemp = str()
        # 提取地址
        self.beginMarkAddress = ['v', 'x', 'p', 'm', 'c', 'home', 's', 'd', 'f', 'date', 'time']
        self.endMarkAddress = ['v', 'x', 's', 'd', 'f', 'c', 'ad', 'home']
        self.addressMark = ['ns', 'nz', 'LOC', 'nt', 'city']
        self.notW = ['-']
        self.dropAddress = ['籍', '住址', '公交车', '动车', '车厢', '列车']
        self.concatAddress: bool = False
        self.hasAddress: bool = False
        self.address = str()
        # 省
        self.province = str()
        # 市
        self.city = str()
        # 区
        self.district = str()
        # 人名
        self.name = list()
        # 活动轨迹文本，存入列表
        self.txt = list()
        # 输出文件名字
        self.outputFilename = str()
        # 结果
        self.result = list()
        # 载入自定义词典
        jieba.load_userdict('tag.txt')
        # 细节匹配
        self.pattern = '[0-9零一二三四五六七八九十百千万亿]+[楼号栋-]+[0-9A-Za-z\u4E00-\u9FA5]*'

    def setPatientName(self, name: list):
        """
        设置患者姓名
        :param name: 患者姓名列表
        :return: 无
        """
        self.name = name
        self.date = str()
        self.time = str()
        self.address = str()

    def setLocation(self, province='', city='', district=''):
        """
        设置默认位置
        :param province: 省
        :param city: 市
        :param district: 区
        :return: 无
        """
        self.province = province
        self.city = city
        self.district = district

    def readPost(self, content=None, filename=None):
        """
        得到活动路径
        :param content: 方式1，文本输入
        :param filename: 方式2，文件读入
        :return: 无
        """
        self.txt = list()
        if filename:
            with open(filename, 'r', encoding='utf-8') as f:
                for line in f.readlines():
                    line = self._stripBrackets(line)
                    self.txt.extend([line])
            self.outputFilename = '提取结果'.join(filename.split('.txt')) + '.csv'
        elif content:
            self.txt = content

    def _needToDrop(self, addr: str) -> bool:
        """
        是否为有效地址
        :param addr: 提取出的地址
        :return: t or f
        """
        for stop in self.dropAddress:
            if stop in addr:
                return True
        return False

    @staticmethod
    def _stripBrackets(line: str) -> str:
        """
        处理多余的括号
        :param line: 原始行
        :return: 处理后的行
        """
        line = line.replace('（', '')
        line = line.replace('）', '')
        line = line.replace('(', '')
        line = line.replace(')', '')
        return line

    def _getDetail(self, addr: str):
        """
        将地址分为小区和住址
        :param addr: 区之后的地址
        :return: 小区，住址
        """
        result = re.findall(self.pattern, addr)
        detail = str()
        if result:
            detail = result[-1]
            addr = addr.replace(detail, '')
        return addr, detail

    def to_csv(self, filename=None) -> None:
        """
        保存到csv
        :param filename: 文件路径
        :return: 无
        """
        output = int()
        if filename:
            output = open(filename, 'w', encoding='utf-8-sig')
        elif self.outputFilename:
            output = open(self.outputFilename, 'w', encoding='utf-8-sig')
        if output:
            output.write('患者,日期,时间,省,市,区,小区,住址,提取地址\n')
            for row in self.result:
                output.write(','.join(row) + '\n')
            output.close()
        self.outputFilename = str()

    def _saveResult(self):
        """
        保存完整的地址结果
        :return: 无
        """
        if self.hasAddress and not self._needToDrop(self.address) and len(self.address) > 1:
            if '钱塘新区' in self.address:
                province, city, distinct = '浙江省', '杭州市', '钱塘新区'
                plot = self.address.split('钱塘新区', 1)[1]
            else:
                df = cpca.transform([self.address], cut=False)
                province, city, distinct, plot = df['省'][0], df['市'][0], df['区'][0], df['地址'][0]
            if province == '':
                province, city, distinct = self.province, self.city, self.district
            plot, detail = self._getDetail(plot)
            if self.name:
                for name in self.name:
                    self.result.append(
                        [name, self.date, self.time, province, city, distinct, plot, detail, self.address])
            else:
                self.result.append(
                    ['', self.date, self.time, province, city, distinct, plot, detail, self.address])
        self.address = str()
        self.hasAddress = False

    def getInfo(self) -> list:
        """
        从文本中提取信息
        :return: 两层的list结果
        """
        self.result = list()
        for sentence in self.txt:
            words = jieba.posseg.cut(sentence, HMM=False)
            for word, flag in words:
                # print('%s %s' % (word, flag))
                # 合并地址
                if str(flag) in self.beginMarkAddress and str(word) not in self.notW:
                    if self.hasAddress:
                        self._saveResult()
                    self.concatAddress = True
                    self.hasAddress = False
                    self.address = str()
                elif self.concatAddress and str(flag) in self.addressMark:
                    self.hasAddress = True
                    self.address += str(word)
                elif self.concatAddress:
                    self.address += str(word)
                # 合并日期
                if str(flag) in self.dateMark:
                    if not self.concatDate:
                        self.dateTemp = str()
                        self.concatDate = True
                    if str(flag) == 'date':
                        self.hasDate = True
                    self.dateTemp += str(word)
                if self.concatDate and (str(flag) not in self.dateMark or str(word) == '日'):
                    self.concatDate = False
                    if self.hasDate:
                        self.time = str()
                        if '月' not in self.dateTemp:
                            self.dateTemp = self.date[0:2] + self.dateTemp
                        self.date = self.dateTemp
                    else:
                        self.dateTemp = str()
                    self.hasDate = False
                # 合并时间
                if str(flag) in self.timeMark or str(word) == ':':
                    if not self.concatTime:
                        self.timeTemp = str()
                        self.concatTime = True
                    if str(flag) == 'time':
                        self.hasTime = True
                    self.timeTemp += str(word)
                elif self.concatTime and str(flag) not in self.timeMark and str(word) != ':':
                    self.concatTime = False
                    if self.hasTime or (len(self.timeTemp) == 5 and self.timeTemp[2] == ':') or (
                            len(self.timeTemp) == 4 and self.timeTemp[1] == ':'):
                        self.time = self.timeTemp
                    else:
                        self.timeTemp = str()
                    self.hasTime = False
        return self.result

    def transform(self, name: list, province='', city='', district='', content=None, inputfile=None,
                  outputfile=None) -> list:
        """
        文本提取信息
        :param name: 默认的姓名列表
        :param province: 默认的省
        :param city: 默认的市
        :param district: 默认的区
        :param content: 文本输入活动轨迹
        :param inputfile: 文件输入活动轨迹
        :param outputfile: 输出csv路径
        :return:
        """
        self.setPatientName(name)
        self.setLocation(province, city, district)
        self.readPost(content, inputfile)
        result = self.getInfo()
        self.to_csv(outputfile)
        return result


if __name__ == '__main__':
    info = Info()

    # 单人
    info.transform(['李某某'], '福建省', '福州市', '鼓楼区', inputfile='demoInfo/福州市鼓楼区李某某.txt')
    info.transform(['方某某'], '福建省', '福州市', '鼓楼区', inputfile='demoInfo/福州市鼓楼区方某某.txt')
    info.transform(['王某'], '福建省', '三明市', '沙县', inputfile='demoInfo/三明市沙县王某.txt')

    # 多人
    info.transform(['赖某', '庄某'], '福建省', '三明市', '永安市', inputfile='demoInfo/三明市永安市赖某庄某.txt')

    # 文本
    txt = '1月19日,自驾车从武汉返回三明市三元区，于1月20日5时左右到三元区莘口镇蓬坑村家中。' \
          '1月20-22日，二人均在家。1月22日下午至23日17时，陈某感觉身体不舒服自行居家休息，期间只接触家人，未与其他人员接触。' \
          '1月23日17时左右,夫妻俩人自驾车前往医院，于18时左右就诊于市中西医结合医院急诊科转发热门诊隔离治疗。' \
          '1月26日晚,患者送往三明市第一医院隔离治疗。 '
    info.transform(['陈某', '姜某'], '福建省', '三明市', '三元区', content=[txt], outputfile='demoInfo/三明市三元区提取结果.csv')
