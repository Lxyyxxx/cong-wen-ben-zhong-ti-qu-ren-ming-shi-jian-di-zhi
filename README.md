# 从文本中提取地址等信息

## 依赖
``` bash
pip install -r requirements.txt
```

## 使用
### 含有患者、日期、时间的提取 `Info.py`
```python
from Info import Info

info = Info()

# 单人
info.transform(['方某某'], '福建省', '福州市', '鼓楼区', inputfile='demo/福州市鼓楼区方某某.txt')  # 该函数会返回两层list的结果
''' 等价于
info.setPatientName(['方某某'])  # 输入患者的姓名，一定要是列表
info.setLocation('福建省', '福州市', '鼓楼区')  # 输入默认的省市区，不输入默认为''
info.readPost(filename='demo/福州市鼓楼区方某某.txt')  # 读入活动轨迹的文件。如果为字符串，见下面的示例
info.getInfo()  # 得到提取结果，该函数会返回两层list的结果
info.to_csv()  # 将结果保存到csv，可以自定义输出路径
'''

# 多人
info.transform(['赖某', '庄某'], '福建省', '三明市', '永安市', inputfile='demo/三明市永安市.txt')
''' 等价于
info.setPatientName(['赖某', '庄某'])
info.setLocation('福建省', '三明市', '永安市')
info.readPost(filename='demo/三明市永安市.txt')
info.getInfo()
info.to_csv()
'''

# 文本
txt = '1月19日,自驾车从武汉返回三明市三元区，于1月20日5时左右到三元区莘口镇蓬坑村家中。' \
      '1月20-22日，二人均在家。1月22日下午至23日17时，陈某感觉身体不舒服自行居家休息，期间只接触家人，未与其他人员接触。' \
      '1月23日17时左右,夫妻俩人自驾车前往医院，于18时左右就诊于市中西医结合医院急诊科转发热门诊隔离治疗。' \
      '1月26日晚,患者送往三明市第一医院隔离治疗。 '
info.transform(['陈某', '姜某'], '福建省', '三明市', '三元区', content=[txt], outputfile='demo/三明市三元区提取结果.csv')
''' 等价于
info.setPatientName(['陈某', '姜某'])
info.setLocation('福建省', '三明市', '三元区')
info.readPost(content=[txt])
info.getInfo()
info.to_csv('demo/三明市三元区提取结果.csv')  # 如果使用文本输入，必须指定输出路径，否则不保存csv
'''
```
### 只有地址的提取 `Address.py`
```python
from Address import Address

address = Address()

# 省市
address.transform('浙江省', '杭州市', inputfile='demoAddress/杭州市.txt')

# 省市区
address.transform('福建省', '三明市', '沙县', inputfile='demoAddress/三明市沙县.txt')

# 文本
txt = '1月19日,自驾车从武汉返回三明市三元区，于1月20日5时左右到三元区莘口镇蓬坑村家中。' \
      '1月20-22日，二人均在家。1月22日下午至23日17时，陈某感觉身体不舒服自行居家休息，期间只接触家人，未与其他人员接触。' \
      '1月23日17时左右,夫妻俩人自驾车前往医院，于18时左右就诊于市中西医结合医院急诊科转发热门诊隔离治疗。' \
      '1月26日晚,患者送往三明市第一医院隔离治疗。 '
address.transform('福建省', '三明市', '三元区', content=[txt], outputfile='demoAddress/三明市三元区提取结果.csv')
```

## 结构
- demoInfo 文件夹，`Info.py`的demo
- demoAddress 文件夹，`Address.py`的demo
- Info.py 提取 患者,日期,时间,省,市,区,小区,住址,提取地址
- Address.py 提取 省,市,区,小区,住址,提取地址
- requirements.txt 依赖库
- tag.txt jieba分词的自定义词库
- README.md

## 说明
结果中的`提取地址`为最初提取到的结果  
经过`cpca`库的映射处理，得到了省、市、区、地址  
然后地址细化为小区和住址（含数字的部分）
