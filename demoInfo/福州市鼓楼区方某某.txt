确诊病例方某某，祖籍湖北武汉，退伍军人，在江苏苏州工业园区做汽车救援工作，福州家庭住址：鼓楼区洪山镇武夷春晓 

　　1月15-21日，从福州回武汉黄陂区老家，在武汉期间参加了同学女儿婚礼，其余时间主要与父母和哥哥一家在武汉老家家中。 

　　1月22-23日，上午08:32乘坐D3263次动车14号车厢（08:32-14:38）回到福州，到福州站后乘坐323路公交车约15:30到家，单独开车前往古田县316国道旁的一个鱼排钓鱼，至23日晚上回到福州家中。 

　　1月24-26日，单人单间居家隔离，离开卧室均有戴口罩，期间未出家门，26日晚出现发热等症状。 

　　1月27日，下午14:00左右独自步行前往联勤保障部队第900医院（简称福州总院）发热门诊就诊，17：00返回到家中。 

　　1月28日-2月3日，单人单间居家隔离，连续服用医院所开药物。 

　　2月4日8:30左右，感觉不适，低热，戴口罩骑共享单车独自往返于福州总院分院和福州总院之间就诊，就诊后由福州总院送分院收住隔离治疗。 