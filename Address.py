# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2020/2/14 13:30.
#

import jieba.posseg
import cpca
import re


class Address:
    def __init__(self):
        # 提取地址
        self.beginMarkAddress = ['v', 'x', 'p', 'm', 'c', 'home', 'date', 'time', 'uj']
        self.addressMark = ['ns', 'nz', 'LOC', 'nt', 'city', 'distinct', 'province']
        self.dropAddress = ['住址', '公交车', '动车', '车厢', '列车', '现在', '人员', '的', '籍', '旅客', '乘客', '车次']
        self.notW = ['-']
        self.concatAddress: bool = False
        self.hasAddress: bool = False
        self.address = str()
        # 省
        self.province = str()
        # 市
        self.city = str()
        # 区
        self.district = str()
        # 载入自定义词典
        jieba.load_userdict('tag.txt')
        # 细节匹配
        self.pattern = '[0-9零一二三四五六七八九十百千万亿]+[楼号栋-]+[0-9A-Za-z\u4E00-\u9FA5]*'
        # 活动轨迹文本，存入列表
        self.txt = list()
        # 输出文件名字
        self.outputFilename = str()
        # 结果
        self.result = list()
        # 标点匹配
        self.punctuation = "[。！？，＂＃＄％＆＇（）＊＋－／：；＜＝＞＠［＼］＾＿｀｛｜｝～｟｠｢｣､、〃》「」『』【】〔〕〖〗〘〙〚〛〜〝〞〟〰〾〿–—‘’‛“”„‟…‧﹏,.……]+"

    def setLocation(self, province='', city='', district=''):
        """
        设置默认位置
        :param province: 省
        :param city: 市
        :param district: 区
        :return: 无
        """
        self.province = province
        self.city = city
        self.district = district

    def readPost(self, content=None, filename=None):
        """
        得到活动路径
        :param content: 方式1，文本输入
        :param filename: 方式2，文件读入
        :return: 无
        """
        self.txt = list()
        if filename:
            with open(filename, 'r', encoding='utf-8') as f:
                for line in f.readlines():
                    line = self._stripBrackets(line)
                    self.txt.extend(re.split(self.punctuation, line))
            self.outputFilename = '提取结果'.join(filename.split('.txt')) + '.csv'
        elif content:
            self.txt = content

    def _needToDrop(self, addr: str) -> bool:
        """
        是否为有效地址
        :param addr: 提取出的地址
        :return: t or f
        """
        for stop in self.dropAddress:
            if stop in addr:
                return True
        return False

    @staticmethod
    def _stripBrackets(line: str) -> str:
        """
        处理多余的括号
        :param addr: 提取出的地址
        :return: 处理后的地址
        """
        line = line.replace('（', '')
        line = line.replace('）', '')
        line = line.replace('(', '')
        line = line.replace(')', '')
        return line

    def _getDetail(self, addr: str):
        result = re.findall(self.pattern, addr)
        detail = str()
        if result:
            detail = result[-1]
            addr = addr.replace(detail, '')
        return addr, detail

    def to_csv(self, filename=None) -> None:
        """
        保存到csv
        :param filename: 文件路径
        :return: 无
        """
        output = int()
        if filename:
            output = open(filename, 'w', encoding='utf-8-sig')
        elif self.outputFilename:
            output = open(self.outputFilename, 'w', encoding='utf-8-sig')
        if output:
            output.write('省,市,区,小区,住址,提取地址\n')
            for row in self.result:
                output.write(','.join(row) + '\n')
            output.close()
        self.outputFilename = str()

    def _saveResult(self):
        """
        保存完整的地址结果
        :return: 无
        """
        if self.hasAddress and not self._needToDrop(self.address) and len(self.address) > 1:
            if '钱塘新区' in self.address:
                province, city, distinct = '浙江省', '杭州市', '钱塘新区'
                plot = self.address.split('钱塘新区', 1)[1]
            else:
                df = cpca.transform([self.address], cut=False)
                province, city, distinct, plot = df['省'][0], df['市'][0], df['区'][0], df['地址'][0]
            if province == '':
                province, city, distinct = self.province, self.city, self.district
            plot, detail = self._getDetail(plot)
            self.result.append([province, city, distinct, plot, detail, self.address])
        self.address = str()
        self.hasAddress = False

    def getInfo(self) -> list:
        """
        从文本中提取信息
        :return: 两层的list结果
        """
        self.result = list()
        for sentence in self.txt:
            words = jieba.posseg.cut(sentence, HMM=False)
            for word, flag in words:
                # print('%s %s' % (word, flag))
                # 合并地址
                if str(flag) in self.beginMarkAddress and str(word) not in self.notW:
                    if self.hasAddress:
                        self._saveResult()
                    self.concatAddress = True
                    self.hasAddress = False
                    self.address = str()
                elif self.concatAddress and str(flag) in self.addressMark:
                    self.hasAddress = True
                    self.address += str(word)
                elif self.concatAddress:
                    self.address += str(word)
            self._saveResult()
        return self.result

    def transform(self, province='', city='', district='', content=None, inputfile=None,
                  outputfile=None) -> list:
        """
        文本提取信息
        :param province: 默认的省
        :param city: 默认的市
        :param district: 默认的区
        :param content: 文本输入活动轨迹
        :param inputfile: 文件输入活动轨迹
        :param outputfile: 输出csv路径
        :return:
        """
        self.setLocation(province, city, district)
        self.readPost(content, inputfile)
        result = self.getInfo()
        self.to_csv(outputfile)
        return result


if __name__ == '__main__':
    address = Address()

    # 省市
    address.transform('浙江省', '杭州市', inputfile='demoAddress/杭州市.txt')

    # 省市区
    address.transform('福建省', '三明市', '沙县', inputfile='demoAddress/三明市沙县.txt')

    # 文本
    txt = '1月19日,自驾车从武汉返回三明市三元区，于1月20日5时左右到三元区莘口镇蓬坑村家中。' \
          '1月20-22日，二人均在家。1月22日下午至23日17时，陈某感觉身体不舒服自行居家休息，期间只接触家人，未与其他人员接触。' \
          '1月23日17时左右,夫妻俩人自驾车前往医院，于18时左右就诊于市中西医结合医院急诊科转发热门诊隔离治疗。' \
          '1月26日晚,患者送往三明市第一医院隔离治疗。 '
    address.transform('福建省', '三明市', '三元区', content=[txt], outputfile='demoAddress/三明市三元区提取结果.csv')
